class CreateUpdatemodels < ActiveRecord::Migration
  def change
    create_table :updatemodels do |t|
      t.string :update_start
      t.string :update_name
      t.string :update_method
      t.string :update_term
      t.string :update_other

      t.timestamps null: false
    end
  end
end
