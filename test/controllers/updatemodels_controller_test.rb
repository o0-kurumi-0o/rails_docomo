require 'test_helper'

class UpdatemodelsControllerTest < ActionController::TestCase
  setup do
    @updatemodel = updatemodels(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:updatemodels)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create updatemodel" do
    assert_difference('Updatemodel.count') do
      post :create, updatemodel: { update_method: @updatemodel.update_method, update_name: @updatemodel.update_name, update_other: @updatemodel.update_other, update_start: @updatemodel.update_start, update_term: @updatemodel.update_term }
    end

    assert_redirected_to updatemodel_path(assigns(:updatemodel))
  end

  test "should show updatemodel" do
    get :show, id: @updatemodel
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @updatemodel
    assert_response :success
  end

  test "should update updatemodel" do
    patch :update, id: @updatemodel, updatemodel: { update_method: @updatemodel.update_method, update_name: @updatemodel.update_name, update_other: @updatemodel.update_other, update_start: @updatemodel.update_start, update_term: @updatemodel.update_term }
    assert_redirected_to updatemodel_path(assigns(:updatemodel))
  end

  test "should destroy updatemodel" do
    assert_difference('Updatemodel.count', -1) do
      delete :destroy, id: @updatemodel
    end

    assert_redirected_to updatemodels_path
  end
end
