# Preview all emails at http://localhost:3000/rails/mailers/model_mailer
class ModelMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/model_mailer/sendmail_confirm
  def sendmail_confirm
    ModelMailer.sendmail_confirm
  end

end
