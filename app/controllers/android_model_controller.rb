require 'rubygems'
require 'nokogiri'
require 'open-uri'

class AndroidModelController < ApplicationController
  def index
    
    update_start = []
    update_name = []
    update_method = []
    update_term = []
    update_other = []
    
# Updatemodel.destroy_all()
    
    doc = Nokogiri::HTML(open("https://www.nttdocomo.co.jp/support/utilization/product_update/list/index.html?s=date"))
    
#    update_start.push(doc.xpath('//*[@headers="cell-update-start-01"]').text)
#    update_name.push(doc.xpath('//*[@headers="cell-update-start-child-01-01 cell-update-name-01"]').text)
#    update_method.push(doc.xpath('//*[@headers="cell-update-start-child-01-01 cell-update-method-01"]').text)
#    update_term.push(doc.xpath('//*[@headers="cell-update-start-child-01-01 cell-update-term-01"]').text)
#    update_other.push(doc.xpath('//*[@headers="cell-update-start-child-01-01 cell-update-others-01"]').text)
#    render text: update_start[]
  
  doc.xpath('//*[@headers="cell-update-start-01"]').each do |content|
    update_start.push(content.text)
  end
  
    doc.xpath('//*[@headers="cell-update-start-child-01-01 cell-update-name-01"]').each do |content|
    update_name.push(content.text)
  end
  
    doc.xpath('//*[@headers="cell-update-start-child-01-01 cell-update-method-01"]').each do |content|
    update_method.push(content.text)
  end
   
  doc.xpath('//*[@headers="cell-update-start-child-01-01 cell-update-term-01"]').each do |content|
  update_term.push(content.text)
  end
  
  doc.xpath('//*[@headers="cell-update-start-child-01-01 cell-update-others-01"]').each do |content|
    update_other.push(content.text)
  end
      
    for i in 0 .. update_start.length do
      updatemodel = Updatemodel.new(
      update_start: update_start[i], 
      update_name: update_name[i],
      update_method: update_method[i], 
      update_term: update_term[i],
      update_other: update_other[i]
    ) 
      updatemodel.save
      updatemodel.errors.full_messages
    end
    
 render text: update_start
  #  updatemodel.save
  #  updatemodel.errors.full_messages
  end
end
