json.array!(@updatemodels) do |updatemodel|
  json.extract! updatemodel, :id, :update_start, :update_name, :update_method, :update_term, :update_other
  json.url updatemodel_url(updatemodel, format: :json)
end
