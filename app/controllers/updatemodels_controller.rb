class UpdatemodelsController < ApplicationController
  before_action :set_updatemodel, only: [:show, :edit, :update, :destroy]

  # GET /updatemodels
  # GET /updatemodels.json
  def index
    @updatemodels = Updatemodel.all
  end

  # GET /updatemodels/1
  # GET /updatemodels/1.json
  def show
  end

  # GET /updatemodels/new
  def new
    @updatemodel = Updatemodel.new
  end

  # GET /updatemodels/1/edit
  def edit
  end

  # POST /updatemodels
  # POST /updatemodels.json
  def create
    @updatemodel = Updatemodel.new(updatemodel_params)

    respond_to do |format|
      if @updatemodel.save
        format.html { redirect_to @updatemodel, notice: 'Updatemodel was successfully created.' }
        format.json { render :show, status: :created, location: @updatemodel }
      else
        format.html { render :new }
        format.json { render json: @updatemodel.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /updatemodels/1
  # PATCH/PUT /updatemodels/1.json
  def update
    respond_to do |format|
      if @updatemodel.update(updatemodel_params)
        format.html { redirect_to @updatemodel, notice: 'Updatemodel was successfully updated.' }
        format.json { render :show, status: :ok, location: @updatemodel }
      else
        format.html { render :edit }
        format.json { render json: @updatemodel.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /updatemodels/1
  # DELETE /updatemodels/1.json
  def destroy
    @updatemodel.destroy
    respond_to do |format|
      format.html { redirect_to updatemodels_url, notice: 'Updatemodel was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_updatemodel
      @updatemodel = Updatemodel.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def updatemodel_params
      params.require(:updatemodel).permit(:update_start, :update_name, :update_method, :update_term, :update_other)
    end
end
