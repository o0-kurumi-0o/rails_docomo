class ModelMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.model_mailer.sendmail_confirm.subject
  #
   default parts_order: ["text/html", "text/enriched", "text/plain"]
    default from: "user@gmail.com"
  def sendmail_confirm(text)
    @greeting = text

    mail to: "shunya.okura@optim.co.jp",subject:"docomoの端末アップデートに関するお知らせです"
  end
end
