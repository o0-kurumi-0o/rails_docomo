require 'rubygems'
require 'nokogiri'
require 'open-uri'
require "date"
    
update_start = []
update_name = []
update_method = []
update_term = []
update_other = []
    
# Updatemodel.destroy_all("update_start == '2016年5月11日'")

    
    doc = Nokogiri::HTML(open("https://www.nttdocomo.co.jp/support/utilization/product_update/list/index.html?s=date"))
    doc.xpath('//*[@headers="cell-update-start-01"]').each do |content|
    update_start.push(content.text)
    end
    
    doc.xpath('//*[@headers="cell-update-start-child-01-01 cell-update-name-01"]').each do |content|
    update_name.push(content.text)
    end
    
    doc.xpath('//*[@headers="cell-update-start-child-01-01 cell-update-method-01"]').each do |content|
    update_method.push(content.text)
    end
    
    doc.xpath('//*[@headers="cell-update-start-child-01-01 cell-update-term-01"]').each do |content|
    update_term.push(content.text)
    end
    
    doc.xpath('//*[@headers="cell-update-start-child-01-01 cell-update-others-01"]').each do |content|
    update_other.push(content.text)
    end
    
    date = Updatemodel.all.order("update_start DESC").first #=> Updatemodelのオブジェクト
    


mailText=""


update_start.each_with_index do |content,i|#=> 比較

    if date.update_start < content then #=> nokogiriの日付が新しい場合
        updatemodel= Updatemodel.new(
            update_start: update_start[i], 
            update_name: update_name[i],
            update_method: update_method[i], 
            update_term: update_term[i],
            update_other: update_other[i]
        ) 
        updatemodel.save
        updatemodel.errors.full_messages
        mailText+="更新開始日時: "+update_start[i]+"\n"+"機種名: "+update_name[i]+"\n"+"対応方法: "+
        update_method[i]+"\n"+"更新期間: "+update_term[i]+"\n"+"その他: "+update_other[i]+"\n\n"

    elsif date.update_start == content then #=> nokogiriの日付が同じ場合   
        db_date_list=[]
        db_date_list = Updatemodel.where(" update_start == " +"'"+ date.update_start+"'")
        
        is_find_db_name=false #=>DB内にあるかどうか判定用フラグ
        
        
        db_date_list.each_with_index do |db_model_name,j| #=>#DB内確認
            if db_model_name.update_name == update_name[i] then
                 is_find_db_name=true
            end
        end 
            if is_find_db_name==false then
            
             updatemodel= Updatemodel.new(
                    update_start: update_start[i], 
                    update_name: update_name[i],
                    update_method: update_method[i], 
                    update_term: update_term[i],
                    update_other: update_other[i]
                ) 
                updatemodel.save
                updatemodel.errors.full_messages
                mailText+="更新開始日時: "+update_start[i]+"\n"+"機種名: "+update_name[i]+"\n"+"対応方法: "+
                update_method[i]+"\n"+"更新期間: "+update_term[i]+"\n"+"その他: "+update_other[i]+"\n\n"
            end
        else
            break   
    end
end

def mail_sender(mailText)
    if mailText!=""
     ModelMailer.sendmail_confirm(mailText).deliver
    end
end
 
 mail_sender(mailText)
 